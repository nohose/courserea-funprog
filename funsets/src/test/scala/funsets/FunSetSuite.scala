package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s5 = singletonSet(5)
    val s7 = singletonSet(7)
    val s1000 = singletonSet(1000)
    val all = union(s1, union(s2, union(s3, union(s5, union(s7, s1000)))))
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singleton(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersection contains common elements") {
    new TestSets {
      val su1 = union(s1, s2)
      val su2 = union(s1, s3)
      val su = intersect(su1, su2)
      assert(contains(su, 1), "Intersect contains 1")
      assert(!contains(su, 2), "Internet does not contain 2")
      assert(!contains(su, 3), "Internet does not contain 3")
    }
  }

  test("filter returns a new set of filtered values") {
    new TestSets {
      val s = union(s1, union(s2, s3))
      assert(contains(filter(s, n => n==3), 3), "Filter 3")
      assert(!contains(filter(s, n => n==3), 2), "Filter 2")

      assert(contains(all, 2))
      assert(!contains(filter(all, n => n % 2 == 1), 2))
      assert(contains(filter(all, n => n % 2 == 1), 7))
    }
  }

  test("forall returns correct value") {
    new TestSets {
      val s = union(s1, s2)
      assert(forall(s, n => n < 3), "Forall n < 3 true")
      assert(!forall(s, n => n < 2), "Forall n < 2 false")
    }
  }

  test("exists returns true if element in set, false otherwise") {
    new TestSets {
      val s = union(s1, s2)
      assert(exists(s, n => n == 2), "Exists n == 2")
      assert(!exists(s, n => n == 3), "Exists n == 3")
      assert(!exists(all, n => n==4), "Exists n == 4")
    }
  }

  test("map returns a new set where all elements are f(a)") {
    new TestSets {
      val s = union(s1, union(s2, s3))
      val t = map(s, n => n * n)
      assert(contains(t, 1), "Contains 1")
      assert(contains(t, 4), "Contains 4")
      assert(contains(t, 9), "Contains 9")
      assert(!contains(t, 2), "Contains 2")
      assert(!contains(t, 3), "Contains 3")
    }
  }
}
